#!/bin/bash

#create directories and start container on the host system

SQUID_USER=proxy
SQUID_CONF=/etc/squid/squid.conf
SQUID_CONF_DIR=/etc/squid/
SQUID_CACHE_DIR=/var/spool/squid
SQUID_LOG_DIR=/var/log/squid

create_cache_dir() {
	mkdir -p cache && \
	chmod -R 755 cache && \
	chown -R "$SQUID_USER:$SQUID_USER" cache
}

create_log_dir() {
	mkdir -p logs && \
	chown -R "$SQUID_USER:$SQUID_USER" logs
}


perm_files() {
	chown -R "$SQUID_USER:$SQUID_USER" cache logs
}

if [ ! -d "cache" ]
then
	echo "Create cache dir"
	create_cache_dir
fi

if [ ! -d "logs" ]
then
	echo "Create log dir"
	create_log_dir
fi

perm_files

echo "start squid"

docker run -p 3128:3128 -d --name squid --restart always \
	-v "/etc/localtime:/etc/localtime:ro" \
	-v "/etc/timezone:/etc/timezone:ro" \
	-v "$(pwd)/conf/squid.conf:$SQUID_CONF" \
	-v "$(pwd)/cache:$SQUID_CACHE_DIR" \
	-v "$(pwd)/logs:$SQUID_LOG_DIR" \
	dedmin/squid
