#!/bin/bash

# Entrypoint for docker squid container

SQUID=`which squid`

create_cache_dir() {
	mkdir -p "$SQUID_CACHE_DIR" && \
	chmod -R 755 "$SQUID_CACHE_DIR" && \
	chown -R "$SQUID_USER:$SQUID_USER" "$SQUID_CACHE_DIR"
}

create_log_dir() {
	mkdir -p "$SQUID_LOG_DIR" && \
	chown -R "$SQUID_USER:$SQUID_USER" "$SQUID_LOG_DIR"
}

perm_files() {
	chown -R "$SQUID_USER:$SQUID_USER" "$SQUID_LOG_DIR" "$SQUID_CACHE_DIR"
}


if [ ! -d "$SQUID_CACHE_DIR" ]
then
	echo "Create cache dir"
	create_cache_dir
fi

if [ ! -d "$SQUID_LOG_DIR" ]
then
	echo "Create log dir"
	create_log_dir
fi

if [ ! -d "$SQUID_CACHE_DIR/00" ] 
then
	echo "Initialize squid cache"
	"$SQUID" -f "$SQUID_CONF" -N -z
fi

perm_files
echo "Start squid"
"$SQUID" -f "$SQUID_CONF" -NYC
